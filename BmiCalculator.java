public class BmiCalculator {
	public static void main(String[] args){
		float w = 0;
		float h = 0;
		float bmi = 0;
		if (args.length != 2){
			System.out.println("To perform the calculation, please give two numeric values: Weight [kg] and Height [m].");
		} else {
			try {
				w = Float.parseFloat(args[0]);
				h = Float.parseFloat(args[1]);
			} catch(Exception e) {
				System.out.println("To perform the calculation, please give two numeric values: Weight [kg] and Height [m].");
				System.exit(0);
			}

			System.out.println("Calculating BMI for weight: " + w + " kg and height: " + h + " m");
			bmi = (w)/(h*h);

			String v = String.format("%.2f", bmi);

			if (bmi < 1) {
				System.out.println("BMI is very low and may be incorrect, \ndid you remember to give weight in kg and height i meters?");
			}
			else if (bmi < 18.5){
				System.out.println("Your BMI is " + v + ". You are underweight.");
			}
			else if(bmi < 24.5){
				System.out.println("Your BMI is " + v + ". You are normal weight.");
			}
			else if(bmi < 29.9){
				System.out.println("Your BMI is " + v + ". You are overweight.");
			}
			else{
				System.out.println("Your BMI is " + v + ". You are obese.");
			}
		}
	}
}